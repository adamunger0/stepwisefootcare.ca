<?php
/* THIS FILE USES PHPMAILER INSTEAD OF THE PHP MAIL() FUNCTION */

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require '../vendor/autoload.php';

/*
 *  CONFIGURE EVERYTHING HERE
 */


$mail = new PHPMailer(true);                                // Passing `true` enables exceptions
try {

    $user_name = $_POST["name"];
    $user_email = $_POST["email"];
    $user_phone = $_POST["phone"];
    $user_message = $_POST["message"];

    // Server settings
    $mail->SMTPDebug = 1;                                   // Enable verbose debug output
    $mail->isSMTP();                                        // Set mailer to use SMTP
    $mail->Host = 'mail.stepwisefootcare.ca';               // Specify main and backup SMTP servers
    //$mail->SMTPAuth = true;                               // Enable SMTP authentication
    //$mail->Username = 'user@example.com';                 // SMTP username
    //$mail->Password = 'secret';                           // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 26;                                       // TCP port to connect to

    // Recipients
    //$mail->addAddress('kate.reddekopp@gmail.com', 'Kathi Reddekopp');     // Add a recipient
    $mail->addAddress('adamunger0@gmail.com', 'Adam Unger');     // Add a recipient
    //$mail->addAddress('xwhiteknuckle22@gmail.com');               // Name is optional
    $mail->addReplyTo($user_email, $user_name);
    $mail->setFrom('admin@stepwisefootcare.ca', 'Admin');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'You have a new message from stepwisefootcare.ca';
    $mail->Body    = 'The contents of the submitted Book or Contact form are as follows:<br>&#09;Name: '.$user_name.'<br>&#09;Email Address: '.$user_email.'<br>&#09;Phone Number: '.$user_phone.'<br>&#09;Message:<br>&#09;&#09;"'.$user_message.'"<br><br>You can use "Reply-To" to directly reply to this enquiry.';
    //$mail->Body    = "The contents of the submitted Book or Contact form are as follows:<br>&#09;Name: {$user_name}<br>&#09;Email Address: {$user_email}<br>&#09;Phone Number: {$user_phone}<br>&#09;Message:<br>&#09;&#09;'{$user_message}'<br><br>You can use 'Reply-To' to directly reply to this enquiry.";
    $mail->AltBody = 'The contents of the submitted Book or Contact form are as follows:%0D%0AName: '.$user_name.'%0D%0AEmail Address: '.$user_email.'%0D%0APhone Number: '.$user_phone.'%0D%0AMessage:%0D%0A"'.$user_message.'"%0D%0A%0D%0AYou can use "Reply-To" to directly reply to this enquiry.';

    $mail->send();
    echo 'Message has been sent';
}
catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
