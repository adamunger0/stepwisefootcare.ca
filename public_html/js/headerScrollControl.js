/* Bind to the document ready function to set the header to the appropriate view. */
$(document).ready(function() {
    checkHeader();
});

/* Bind to the scroll function to dynamically change the header in response to
   the current scroll position.
 */
$(window).scroll(function() {
    checkHeader();
});

/* Bind to the resize function to dynamically respond to varying screen widths
   for ensuring the correct header is displayed.
 */
$(window).resize(function() {
    checkHeader();
});

/* Change to a 'pretty' header if the scroll is at the top of the screen and if
   we are being viewed on a large screen.
 */
function checkHeader() {
    // If we're on a small screen (width < 768px; a number defined by Bootstrap)
    // or if we are scrolling away from the top of the screen (scrollTop > 50),
    // then make the header plain-jane. Note that the width() function may
    // return a value that doesn't exactly match what the css rules are using
    // due to differences in each browser (i.e. with/without scroll bars, etc).
    if ($(window).width() < 768 || $(document).scrollTop() > 50) {
        //console.log("Make not fancy.");
        $('.navbar').removeClass('navbar-fancy');
        $('.navbar-inverse .navbar-nav li a').css('font-size', '1.5rem');
        $('.navbar-inverse .navbar-nav li a').css('line-height', '4rem');
        $('.header-branding-name').removeClass('header-branding-name-fancy').addClass('header-branding-name-not-fancy');
        $('.header-branding-number').removeClass('header-branding-number-fancy');
    }
    // If we're here then this site is being viewed on a large screen and
    // we are at the top of the scroll; make a fancy header.
    else {
        //console.log("Make fancy.");
        $('.header-branding-name').removeClass('header-branding-name-not-fancy').addClass('header-branding-name-fancy');
        $('.header-branding-number').addClass('header-branding-number-fancy');
        $('.navbar-inverse .navbar-nav li a').css('font-size', '2rem');
        $('.navbar-inverse .navbar-nav li a').css('line-height', '7.8rem');
        $('.navbar').addClass('navbar-fancy');
    }
}

/*
var output = document.getElementById("output");
var date = new Date();
var displayTime = date.getHours() + ":" + date.getMinutes() + " " + date.getSeconds() + "s " + date.getMilliseconds() + "ms";
var fs = $(".header-branding-name").css("font-size");

output.innerHTML = displayTime + "<br>header-branding-name font-size...." + fs;
*/
