$(function () {
    var validator = $('#contact-form').validate({
        groups: {
            contact_details_group: "email phone"
        },
        rules: {
            name: "required",
            email: {
                require_from_group: [1, ".contact-details-group"]
            },
            phone: {
                require_from_group: [1, ".contact-details-group"]
            },
            message: "required"
		},
        //errorElement: "em",
        // Place the error messages below the input field.
		errorPlacement: function (error, element) {
            //console.log( element.attr('id') );
			// Add the `help-block` class to the error element for styling information
            //if (element.attr('id') === 'form_email' || element.attr('id') === 'form_phone') {
            //    error.addClass("help-block");
            //    error.addClass("help-block-email-phone");
			//    error.insertAfter(element);
            //}
            //else {
			    error.addClass("help-block");
			    error.insertAfter(element);
            //}
		},
		success: function (label, element) {
            //console.log('Success!!!!!!');
            $(element).nextAll('.form-control-feedback').show().removeClass('glyphicon-remove').addClass('glyphicon-ok');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
        highlight: function (element, errorClass, validClass) {
            //console.log('Highlight');
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).nextAll('.form-control-feedback').show().removeClass('glyphicon-ok').addClass('glyphicon-remove');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function (element, errorClass, validClass) {
            //console.log('Unhighlight');
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).nextAll('.form-control-feedback').show().removeClass('glyphicon-remove').addClass('glyphicon-ok');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		}
    });


    // when the form is submitted
    $('#contact-form').on('submit', function (e) {
        //console.log('The contact form submit button has been clicked. Preparing to validate....');
        // if the validator does not prevent form submit
        if ($("#contact-form").valid()) {
            //console.log('The validator has deemed the form information as valid.....');
            var url = "/php/contact.php";
            //console.log($(this).serialize());

            // POST values in the background the the script URL
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data) {
                    //console.log("Ajax return:", data);
                    //console.log("success");
                    // reset the form
                    //validator.resetForm();
                    //$('#contact-form')[0].reset();
                    location.reload(true);
                },
                error: function(exception){ 
                    //alert('Exception:' + exception);
                    console.log("Exception sending form data:");
                    console.log(exception);
                }
            });
            //console.log("The ajax post is complete.");
            return false;
        }
    })
});
