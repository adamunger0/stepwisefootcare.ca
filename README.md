# stepwisefootcare.ca
Website for stepwisefootcare.ca

Uses [Jquery 1.12.4](http://api.jquery.com/).

Uses the [3.3.7 Bootstrap](https://getbootstrap.com/docs/3.3/) library.

Uses the [Jquery Validator](https://jqueryvalidation.org/) plugin.

The [PHPMailer](https://github.com/PHPMailer/PHPMailer.git) stuff was installed via [Composer](https://getcomposer.org/).
